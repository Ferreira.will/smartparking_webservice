//https://api.jquery.com/

// Get the input box
var textInput = document.getElementById("plnumber");

// Init a timeout variable to be used below
var timeout = null;

function func(textInput){
    setInterval(function(){  
        console.log('Input Value:', textInput.value()); }, 500);
}

// Listen for keystroke events
textInput.onchange = function (e) {

    // Clear the timeout if it has already been set.
    // This will prevent the previous task from executing
    // if it has been less than <MILLISECONDS>
    clearTimeout(timeout);

    // Make a new timeout set to go off in 800ms
    timeout = setTimeout(function () {
       
    }, 500);
};