import json
import datetime 
from asgiref.sync import async_to_sync
from django.utils.dateparse import parse_date, parse_time
from channels.generic.websocket import WebsocketConsumer
from .models import (Entrada, Atributo, Usuario, 
                    Movimentacao, Veiculo,Estacionamento)



class ChatConsumer(WebsocketConsumer):
    

    def get_incoming(self, data):
        last_incoming = Entrada.last_incoming()

        content = {
            'parameter' : 'last_incoming',
            'last_incoming' : self.incoming_to_json(last_incoming)
        }
        self.send_message(content)

    
    def get_leaving(self,data):
        print("Leaving")
        pass

    def update_registers(self,data):
        #https://books.agiliq.com/projects/django-orm-cookbook/en/latest/datetime.html
        #https://books.agiliq.com/en/latest/
        print(data['dados']['usr_data'])
        pl_report = data['dados']['parameter']
        usr_report = data['dados']['usr_data']
        vec = Veiculo.objects.get(placa = pl_report)
        park = Estacionamento.objects.get(numpark = 1)
        print(park)
        print(vec)
        ident = Usuario.objects.get(nome = usr_report['nome'],sobrenome = usr_report['sobrenome'])
        print(ident)
        Mov = Movimentacao(codus = ident,
                           codvei= vec,
                           codpark= park  )
        try:    
            last_saved = Movimentacao.get_last().values('codus__nome')[0]
            if last_saved['codus__nome'] == usr_report['nome']:
                print("Do nothing")
            else:
                Mov.save()
        except:
            print("banco vazio")
            Mov.save()

        content = {
            'parameter' : 'register_updated',
            'register_updated' : 'Need_Erase'
        }
        self.send_message(content)



    def get_registers(self,data):
        print("................ ..................................")
        print("Está em get registers")
        print(data['nome'].split("_"))
        usr_data = Usuario.identifier(data['nome'].split("_")[0],
                   data['nome'].split("_")[1]).values('nome',
                                                     'sobrenome',
                                                     'matricula',
                                                     'tipo__ocupacao',
                                                     'statususu__status')
        print(usr_data)
        print("...................................................")
        content = {  
            'parameter' : 'get_registers',
            'usr_data' : self.registers_to_json(usr_data)
        }
        print(content)
        self.send_message(content)
         

    def show_users(self,data):
        related_users = Atributo.all_drivers(data['placa']).values('codusu__nome',
                                                                    'codusu__sobrenome',
                                                                    'codusu__foto')
        content = {
            'parameter' : 'show_users',
            'related_users' : self. queryset_users_json(related_users)
        }

        self.send_message(content)
        
        
    def queryset_users_json(self, users):
        result = []
        for user in users:
            result.append(self.user_to_json(user))
        return result

    def user_to_json(self, user):
        return({
            'nome': user['codusu__nome'],
            'sobrenome': user['codusu__sobrenome'],
            'foto': user['codusu__foto']
        })
    

    def incoming_to_json(self, message):
        user_data = message.values()[0]
        placa = user_data['placa']
        return({
            'placa': user_data['placa'],
            'img': user_data['img'],
            'moment': str(user_data['moment'])

        })
    
    def registers_to_json(self,message):
        return({
            'nome': message[0]['nome'],
            'sobrenome':message[0]['sobrenome'],
            'matricula':message[0]['matricula'],
            'ocupacao':message[0]['tipo__ocupacao'],
            'status':message[0]['statususu__status'],


        })
         
    
    commands = {
        'get_incoming'     :     get_incoming,
        'get_leaving'      :      get_leaving,
        'get_registers'    :    get_registers,
        'show_users'       :       show_users,
        'update_registers' : update_registers,
    }



    def connect(self):
        self.accept()

    def disconnect(self, close_code):
        pass

    # Receive message from WebSocket

    def receive(self, text_data):
        data = json.loads(text_data)
        print("------------------------------------------------------")
        print("O dado recebido do websocket foi:")
        print(data)
        print("------------------------------------------------------")
        self.commands[data['command']](self, data)
        #time.sleep(1)
        self.close()



    def send_message(self, message):
        self.send(text_data=json.dumps(message))

    # Receive message from room group
    def chat_message(self, event):
        message = event['message']
        self.send(text_data=json.dumps(message)) 