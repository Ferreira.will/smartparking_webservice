from django.urls import path,re_path
from .views import user_list,home,room

urlpatterns = [
    path('',home),
    path('users/',user_list),
    re_path(r'^(?P<room_name>[^/]+)/$',room, name='room'),
]
