from django.urls import re_path

from .consumers import ChatConsumer

websocket_urlpatterns = [
    # ws://127.0.0.1:8000/ws/core/room/
    re_path(r'^ws/core/room/$', ChatConsumer),
]