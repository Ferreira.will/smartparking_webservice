from django.http import JsonResponse
from django.shortcuts import render
from django.utils.safestring import mark_safe
import json


def user_list(request):
    context = {
        "name" : 'William Ferreira',
        "plate": 'ABC-1234'
    }
    return JsonResponse(context)

def home(request):
    return render(request,'core/refact.html',{})

# Not used more
def room(request, room_name):
    return render(request, 'core/refact.html', {
        'room_name_json': mark_safe(json.dumps(room_name))
    })
