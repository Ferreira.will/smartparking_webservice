from django.contrib import admin
from django.contrib.admin import AdminSite
from .models import (Localizacao, Campus, Estacionamento,
                    TipoPessoa, StatusCategoria, StatusPessoa, 
                    Marca, Propietario, Usuario, Veiculo, 
                    Atributo, Relacao, Movimentacao,Entrada)

class MyAdminSite(AdminSite):
    admin.site.site_header = 'Administração do Smartparking'
    
admin_site = MyAdminSite()

class ShowAtributo(admin.ModelAdmin):
    list_display = ('Placa','Veiculo','Usuarios','Propietario')
    search_fields = ('codcar__placa',)
    #list_filter = ('codcar__propietario__nome',)



    def Propietario(self, obj):
        return obj.codcar.propietario.nome  + " " + obj.codcar.propietario.sobrenome

    def Usuarios(self, obj):
        return obj.codusu.nome  + " " + obj.codusu.sobrenome
    
    def Placa(self, obj):
        return obj.codcar.placa
    
    def Veiculo(self, obj):
        return obj.codcar.marca.marca + " " + obj.codcar.modelo
    
class ShowUsuario(admin.ModelAdmin):
    search_fields = ('nome',)
    list_display = ('Nome','Ocupacao','Status','Tempo')
    list_filter = ('statususu__status__categoria',)

    def Nome(self,obj):
        return obj.nome + " " + obj.sobrenome
    
    def Ocupacao(self,obj):
        return obj.tipo.ocupacao

    def Status(self,obj):
        return obj.statususu.status.categoria
    
    def Tempo(self,obj):
        return obj.statususu.tempo

class ShowMovimentacao(admin.ModelAdmin):
    list_display = ('Usuario','Placa','Veiculo','Entrada','Saida')

    
    def Entrada(self,obj):
        return obj.datein

    def Saida(self,obj):  
        return obj.dateout

    def Usuario(self, obj):
        return obj.codus.nome  + " " + obj.codus.sobrenome
    
    def Placa(self, obj):
        return obj.codvei.placa
    
    def Veiculo(self, obj):
        return obj.codvei.marca.marca + " " + obj.codvei.modelo





admin.site.register(Usuario, ShowUsuario)
admin.site.register(Atributo, ShowAtributo)
admin.site.register(Movimentacao, ShowMovimentacao)


# Register your models here.
admin.site.register(Entrada)
admin.site.register(Localizacao)
admin.site.register(Campus)
admin.site.register(Estacionamento)
admin.site.register(TipoPessoa)
admin.site.register(StatusPessoa)
admin.site.register(StatusCategoria)
admin.site.register(Propietario)
admin.site.register(Marca)
admin.site.register(Veiculo)
admin.site.register(Relacao)
