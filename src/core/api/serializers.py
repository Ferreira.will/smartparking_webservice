from rest_framework import serializers
from core.models import Localizacao,Veiculo,Entrada

class LocalizacaoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Localizacao
        fields = ('nomecidade','nomeestado')

class VeiculoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Veiculo
        fields = ('placa', 'marca')

class EntradaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Entrada
        fields = ('id','placa','moment', 'img')



