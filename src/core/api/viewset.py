from .serializers import LocalizacaoSerializer, VeiculoSerializer, EntradaSerializer
from rest_framework import generics
from rest_framework.permissions import IsAdminUser
from core.models import Localizacao,Veiculo,Entrada


class LocList(generics.ListCreateAPIView):
    queryset = Localizacao.objects.all()
    serializer_class = LocalizacaoSerializer

class VeiculoList(generics.ListCreateAPIView):
    queryset = Veiculo.objects.all()
    serializer_class = VeiculoSerializer


class EntradaList(generics.ListCreateAPIView):
    queryset = Entrada.objects.all()
    serializer_class = EntradaSerializer

