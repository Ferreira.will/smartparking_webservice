from django.urls import path,re_path

from .viewsets import LocList, VeiculoList

urlpatterns = [
    path('loc/',LocList),
    path('veiculo/',VeiculoList),
]