from django.db import models



# Create your models here.
class Localizacao(models.Model):
    nomecidade = models.CharField(max_length=50, blank=False, null=False)
    nomeestado = models.CharField(max_length=50, blank=False, null=False)

    def __str__(self):
        return self.nomecidade + " de " + self.nomeestado

class Campus(models.Model):
    cidade = models.ForeignKey(Localizacao, on_delete=models.PROTECT)
    nomcampus = models.CharField(max_length=42, blank=False, null=False)
    qtpark = models.IntegerField()
    enderecocampus = models.CharField(max_length=100, blank=False, null=False)

    def __str__(self):
        return (self.nomcampus + " de " + self.cidade.nomecidade + 
                " | Número de estacionamentos: " + str(self.qtpark))
    

class Estacionamento(models.Model):
    campus = models.ForeignKey(Campus, on_delete=models.PROTECT)
    numpark = models.IntegerField()
    numvagas= models.IntegerField()

    def __str__(self):
        return (self.campus.nomcampus + 
            " Estacionamento: "+ str(self.numpark) + 
            " | Número de vagas: " + str(self.numvagas))

    def getpark():
        return Estacionamento.objects.filter(id = 1)

class TipoPessoa(models.Model):
    ocupacao = models.CharField(max_length=20, blank=False, null=False)

    def __str__(self):
        return self.ocupacao

class StatusCategoria(models.Model):
    categoria = models.CharField(max_length=20, blank=False, null=False)

    def __str__(self):
        return self.categoria

class StatusPessoa(models.Model):
    status = models.ForeignKey(StatusCategoria, on_delete = models.PROTECT)
    desc = models.TextField(max_length= 140,blank=True, null=True)
    tempo = models.DurationField(blank=True, null=True)

    def __str__(self):
        return str(self.status) 

class Marca(models.Model):
    marca = models.CharField(max_length=20, blank=False, null=False)

    def __str__(self):
        return self.marca

class Propietario(models.Model):
    nome = models.CharField(max_length=42, blank=False, null=False)
    sobrenome = models.CharField(max_length=42, blank=False, null=False)
    matricula = models.CharField(max_length=10, blank=False, null=False)
    cpf = models.CharField(max_length=11, blank=False, null=False)
    sexo = models.CharField(max_length=15, blank=False, null=False)

    def __str__(self):
        return self.nome + " " + self.sobrenome + " Matrícula: " + self.matricula

class Usuario(models.Model):
    statususu = models.ForeignKey(StatusPessoa, on_delete=models.PROTECT)
    tipo = models.ForeignKey(TipoPessoa, on_delete = models.PROTECT)
    nome = models.CharField(max_length=42, blank=False, null=False)
    sobrenome = models.CharField(max_length=42, blank=False, null=False)
    matricula = models.CharField(max_length=10, blank=False, null=False)
    cpf = models.CharField(max_length=11, blank=False, null=False)
    sexo = models.CharField(max_length=15, blank=False, null=False)
    foto = models.ImageField()

    def __str__(self):
        return self.nome + " " + self.sobrenome 

    def identifier(nome, sobrenome):
        return Usuario.objects.filter(nome = nome, sobrenome = sobrenome)
    


class Veiculo(models.Model):
    marca = models.ForeignKey(Marca, on_delete=models.PROTECT)
    propietario = models.ForeignKey(Propietario, on_delete=models.PROTECT)
    placa = models.CharField(max_length=7, blank=False, null=False)
    cor = models.CharField(max_length=42, blank=False, null=False)
    modelo = models.CharField(max_length=42, blank=False, null=False)
    renav = models.CharField(max_length=11, blank=False, null=False)

    def __str__(self):
        return self.marca.marca + " " + self.modelo + " | Placa: " + self.placa
    
    def get_vec(placa):
        return Veiculo.objects.filter(placa = placa)

class Atributo(models.Model):
    codusu = models.ForeignKey(Usuario, on_delete=models.PROTECT)
    codcar = models.ForeignKey(Veiculo, on_delete=models.PROTECT)

    def __str__(self):
        return (self.codusu.nome + " Vinculado ao veículo: " + self.codcar.modelo 
                + " | Placa: " +self.codcar.placa)
    def all_drivers(new_placa):
        return Atributo.objects.filter(codcar__placa= new_placa)
        

class Relacao(models.Model):
    codpes = models.ForeignKey(Usuario, on_delete=models.PROTECT)
    codprop = models.ForeignKey(Propietario, on_delete=models.PROTECT)
    parentesco = models.CharField(max_length=42, blank=False, null=False)

    def __str__(self):
        return (self.codpro.nome + " é " + self.parentesco 
                + " de " +self.codpes.nome)

class Movimentacao(models.Model):
    codus = models.ForeignKey(Usuario, on_delete=models.PROTECT)
    codvei = models.ForeignKey(Veiculo, on_delete=models.PROTECT)
    codpark = models.ForeignKey(Estacionamento, on_delete=models.PROTECT)
    datein = models.DateField(blank=False, null=False, auto_now=True)
    hourin = models.TimeField(blank=False, null=False, auto_now=True)
    dateout = models.DateField(blank=True, null=True)
    hourout = models.TimeField(blank=True, null=True)

    def __str__(self):
        return (self.codus.nome +
                "_" + self.codvei.placa)

    def get_last():
        return Movimentacao.objects.order_by('-id').all()[:1]

class Entrada(models.Model):
    placa = models.CharField(max_length=7, blank=False, null=False)
    moment = models.DateTimeField(auto_now_add=True)
    img = models.CharField(max_length=100, blank=False, null=False)

    def __str__(self):
        return self.placa

    def last_incoming():
        return Entrada.objects.order_by('-id').all()[:1]
