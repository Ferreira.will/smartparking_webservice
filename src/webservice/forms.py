from django.forms import ModelForm
from core.models import Movimentacao

class RelatorioForm(ModelForm):
    class Meta:
        model = Movimentacao
        fields = '__all__'
