"""webservice URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,re_path,include
from rest_framework import routers
from .views import home, report
from core import views
from core.api.viewset import LocList, VeiculoList,EntradaList
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('', home,name = 'official_home'),
    path('report/',report, name = 'relatorios'),
    path('api/loc/',LocList.as_view()),
    path('api/veiculo/',VeiculoList.as_view()),
    path('api/entrada/',EntradaList.as_view()),
    path('core/', include('core.urls')),
    path('admin/', admin.site.urls),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
