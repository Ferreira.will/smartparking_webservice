# SmartParking Webservice

Com a implementação do SmartParking na PUC Minas, almeja-se evitar que um mesmo
usuário entre com vários veículos diferentes, use vagas destinadas a categorias de usuários com
prioridade ou preferência e que pessoas não vinculadas a universidade usem o estacionamento
enquanto realizando atividades fora do campus.

### 1. Links

[Video de funcionamento do protótipo](http://drive.google.com/file/d/1aIxMbNOjwkYAFEuCUT0qV24dNa2lxC4y/view?usp=sharing "project video")  
[Projeto SmartParking Client](https://gitlab.com/Ferreira.will/smartparking "client side project")


### 2. Técnologias Utilizadas:

*   Python
*   Django
*   Django REST framework
*   Django Channels
*   Javascript
*   WebSockets
*   HTML/CSS

### 3. Utilização

Execute o shell script ***runserver*** para ativar o ambiente de desenvolvimento

```bash
$sudo chmod +x runserver.sh
$sudo runserver.sh
```

Para verificar o funcionamento acesse o [localhost](http://127.0.0.1:8080 "  ")