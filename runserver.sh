#!/bin/bash

echo "           ###### Activating Virtual Enviroment ######"
source webenv/bin/activate
echo "           ###### Virtual Enviroment Activated ######"

echo "           ###### Initializing Docker Img ######"
sudo docker run -p 6379:6379 -d redis:2.8
echo "           ###### Redis Server Started ######"

echo "           ###### Starting WebService ######"
python src/manage.py runserver

